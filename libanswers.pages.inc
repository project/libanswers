<?php

/**
 * @file
 * Libanswers non-admin pages
 */


/**
 * Page callback function from hook_menu().
 * The URL will pass in parameters. The 1st should be the question ID.
 */
function libanswers_page_get_answer($question_id) {
  // Add the search form at the top of the search results (no tag cloud).
  $output = render(drupal_get_form('libanswers_search_form'));

  if (!empty($question_id) && is_numeric($question_id)) {
    $answer_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/faqs/' . $question_id . '?iid=' . LIBANSWERS_ID));
    $answer .= '<h2>' . $answer_object->faqs[0]->question . '</h2>';
    $answer .= '<div class="la_updated">Updated: ' . $answer_object->faqs[0]->updated . '</div>';
    if (!empty($answer_object->faqs[0]->topics)) {
      $answer .= '<div class=la_topiclist><b>Topics:</b> ';
      $counter = 0;
      foreach($answer_object->faqs[0]->topics as $topic) {
        if ($counter > 0) { $answer .= ', '; }
        $answer .= '<a href="/ask-us/topic/' . $topic->id . '">' . $topic->name . '</a>';
        $counter++;
      }
      $answer .= '</div>';
    }
    $answer .= '<div class="la_answer">' . $answer_object->faqs[0]->answer . '</div>';
    // Remove font & span tags.
    $answer = preg_replace('/(<font[^>]*>)|(<\/font>)|(<span[^>]*>)|(<\/span>)/', '', $answer);
    $output .= $answer;
  }
  else {
    $output .= t('No answer was found for the given question.');
  }

  $header = FALSE; // Show the secondary header text for answer pages.
  // Add the question form.
  $output .= libanswers_get_question_form($header);
  // Add the tag cloud at the bottom of the search results.
  $output .= libanswers_get_tag_cloud();

  return $output;
}

/**
 * Page callback function from hook_menu().
 * Shows the knowledgebase type search form.
 */
function libanswers_search_form() {
  $minimal_domain = str_replace('http://', '', str_replace('https://', '', LIBANSWERS_DOMAIN));
  $output = '';
  $output .= '<link rel="stylesheet" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/themes/base/jquery-ui.css">' . "\n";
  $output .= '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>' . "\n";
  $output .= '<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js"></script>' . "\n";
  /*if (!empty($query_encoded)) {
    $answers_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/search/' . $query_encoded . '?iid=' . LIBANSWERS_ID . '&limit=20'));
    if ($answers_object->search->numFound > 0) {
      $answers = '<div id="la_search_results"><ul>';
      foreach ($answers_object->search->results as $answer) {
        $answers .= '<li><a href="/ask-us/answer/' . $answer->id . '">' . $answer->question . '</a></li>';
      }
      $answers .= '</ul></div>';
      $output .= t('<h2>Answers related to: "') . $query . '"</h2>' . $answers;
    }
    else {
      $output .= t('No answers were found for the given search.');
    }
  }*/
  $output .= '<script type="text/javascript">
    (function ($) {
    $(document).ready(function(){

      $("#edit-libanswers-autocomplete").autocomplete({
        source: function(request, response) {
          $.ajax({
            url: "//api2.libanswers.com/1.0/search/" + request.term + "?iid=' . LIBANSWERS_ID . '&limit=20",
            dataType: "jsonp",
            success: function(data) {
              //console.log(data.data.search.results);
              response($.map(data.data.search.results, function(item) {
                return {
									label: item.question,
									value: item.question,
									qid: item.id
								}
              }));
            }
          });
        },
        minLength: 3,
        select: function(event, ui) {
          if (ui.item.qid > 0) {
						window.open("/ask-us/answer/"+ui.item.qid, "_self");
					}
					else {
						return false;
					}
        },
        open: function() {
          $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
          // Fix the position when the list is opened.
          var position = $("#edit-libanswers-autocomplete").offset(), left = position.left, top = position.top;
          var height = $("#edit-libanswers-autocomplete").height();
          $("ul.ui-autocomplete").css({
            left: left + "px",
            top: top + height + 4 + "px"
          });
        },
        close: function() {
          $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
      });
      // Below is just a fix for an error with jquery UI.
      // $.curCSS is not a function
      $.curCSS = $.css;
    });
    })(jQuery);
  </script>';
  $form['#prefix'] = $output;
  $form['libanswers_autocomplete'] = array(
    '#type' => 'textfield',
    '#title' => '',
    '#size' => 40,
    '#prefix' => '<div class="ui-widget">',
    '#suffix' => '</div>',
    '#default_value' => t('Search Frequently Asked Questions'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
  );
  if ($_GET['q'] == 'ask-us') {
    $form['#suffix'] = '';
    $form['#suffix'] .= libanswers_get_question_form();
    $form['#suffix'] .= libanswers_get_tag_cloud();
    $form['#suffix'] .= '<h2>Featured Answers</h2>';
    $questions = '';
    // Pull from 24 hr cache.
    $cid = 'libanswers:featured-10';
    $cached = cache_get($cid, 'cache');
    if ($cached && time() < $cached->expire) {
      $questions_json = $cached->data;
    }
    else {
      // Not in cache. Let's run the query.
      // Make API call.
      $questions_json = file_get_contents('https://api2.libanswers.com/1.0/faqs?iid=' . LIBANSWERS_ID . '&topic_id=' . LIBANSWERS_FEATURED_TOPIC_ID . '&limit=10&sort=totalhits&sort_dir=desc');
      cache_set($cid, $questions_json, 'cache', time() + 86400); // Cache the query result for 24 hours.
    }
    $questions_object = json_decode($questions_json);
    if (!empty($questions_object->faqs)) {
      $questions .= '<ul class="la_anslist">';
      foreach ($questions_object->faqs as $question) {
        $questions .= '<li><a class="la_question" href="/ask-us/answer/' . $question->faqid . '">' . $question->question . '</a></li>';
      }
      $questions .= '</ul>';
    }
    $form['#suffix'] .= $questions;
    $libanswers_description = variable_get('libanswers_description');
    if (!empty($libanswers_description)) {
      $form['#suffix'] .= $libanswers_description;
    }
  }
  return $form;
}

/**
 * Submit handler for form ID 'libanswers_search_form'.
 */
function libanswers_search_form_submit($form, &$form_state) {
  // Redirect to results page.
  $form_state['redirect'] = array(
    'ask-us/search-results', array('query' => array(
      'query' => $form_state['values']['libanswers_autocomplete'],
    ))
  );
}

/**
 * Page title callback function from hook_menu().
 * The URL will pass in parameters. The 1st should be the topic ID.
 */
function libanswers_page_get_topic_title($topic_id) {
  $topic_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/topics/' . $topic_id . '?iid=' . LIBANSWERS_ID));
  return ucwords($topic_object->topics[0]->name);
}

/**
 * Page callback function from hook_menu().
 */
function libanswers_page_get_topic_list() {
  // Add the search form at the top of the search results (no tag cloud).
  $output = render(drupal_get_form('libanswers_search_form'));
  $topics_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/topics?iid=' . LIBANSWERS_ID . '&sort=name&limit=100'));
  if (!empty($topics_object->topics)) {
    $output .= '<ul>';
    foreach ($topics_object->topics as $topic) {
      $output .= '<li><a href="/ask-us/topic/' . $topic->topic_id . '">' . $topic->name . '</a></li>';
    }
    $output .= '</ul>';
  }
  else {
    $output .= t('No topics were found.');
  }

  return $output;
}

/**
 * Page callback function from hook_menu().
 *
 * The URL will pass in parameters. The 1st should be the topic ID.
 */
function libanswers_page_get_topic_answers($topic_id) {
  // Add the search form at the top of the search results (no tag cloud).
  $output = render(drupal_get_form('libanswers_search_form'));

  if (!empty($topic_id) && is_numeric($topic_id)) {
    $answers_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/faqs?iid=' . LIBANSWERS_ID . '&topic_id=' . $topic_id . '&limit=50'));
    if (!empty($answers_object->faqs)) {
      $answers .= '<ul>';
      foreach ($answers_object->faqs as $question) {
        $answers .= '<li><a href="/ask-us/answer/' . $question->faqid . '">' . $question->question . '</a></li>';
      }
      $answers .= '</ul>';
    }
    $output .= '<h2>' . t('Answers related to: "') . libanswers_page_get_topic_title($topic_id) . '"</h2>' . $answers;
  }
  else {
    $output .= t('No answers were found for the given topic.');
  }

  // Add the question form.
  $output .= libanswers_get_question_form();
  // Add the tag cloud at the bottom of the search results.
  $output .= libanswers_get_tag_cloud();

  return $output;
}

/**
 * Page callback function from hook_menu().
 */
function libanswers_search_results() {
  // Add the search form at the top of the search results (no tag cloud).
  $output = render(drupal_get_form('libanswers_search_form'));

  $query = check_plain($_REQUEST['query']);
  $query_encoded = urlencode(urldecode($query)); // Get it into the right format for the LibAnswers API submission.
  if (!empty($query_encoded)) {
    $answers_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/search/' . $query_encoded . '?iid=' . LIBANSWERS_ID . '&limit=20'));
    if ($answers_object->search->numFound > 0) {
      $answers = '<div id="la_search_results"><ul>';
      foreach ($answers_object->search->results as $answer) {
        $answers .= '<li><a href="/ask-us/answer/' . $answer->id . '">' . $answer->question . '</a></li>';
      }
      $answers .= '</ul></div>';
      $output .= t('<h2>Answers related to: "') . $query . '"</h2>' . $answers;
    }
    else {
      $output .= t('No answers were found for the given search.');
    }
  }
  else {
    $output .= t('No answers were found for the given search.');
  }

  // Add the question form.
  $output .= libanswers_get_question_form();
  // Add the tag cloud at the bottom of the search results.
  $output .= libanswers_get_tag_cloud();

  return $output;
}

/**
 * Page callback function from hook_menu().
 * Same as the function above, but only prints the HTML and exits. No themed page is created. Just a partial page for use by the Javascript.
 * e.g. /ask-us/search-results-unthemed?query=hours
 */
function libanswers_search_results_unthemed() {
  $query = check_plain($_REQUEST['query']);
  $query_encoded = urlencode(urldecode($query)); // Get it into the right format for the LibAnswers API submission.
  if (!empty($query_encoded)) {
    $answers_object = json_decode(file_get_contents('https://api2.libanswers.com/1.0/search/' . $query_encoded . '?iid=' . LIBANSWERS_ID . '&limit=20'));
    if ($answers_object->search->numFound > 0) {
      $answers = '<div id="la_search_results"><ul>';
      foreach ($answers_object->search->results as $answer) {
        $answers .= '<li><a href="/ask-us/answer/' . $answer->id . '">' . $answer->question . '</a></li>';
      }
      $answers .= '</ul></div>';
      print '<h2>' . t('Is this what you need?') . '</h2>' . $answers . '<p><a href="/ask-us">' . t('Show me more') . ' &raquo;</a></p><a href="javascript:;" class="button" id="libanswers-please-send">' . t('No. Please send.') . '</a>';
    }
    else {
      print '<h2>' . t('No Answers found') . '</h2><p>' . t('No relevant answers to your question were found in the system.') . '<p><a href="/ask-us">' . t('Show me other answers') . ' &raquo;</a></p><a href="javascript:;" class="button" id="libanswers-please-send">' . t('Please send my question.') . '</a>';
    }
  }
  else {
    print t('No answers were found for the given search.');
  }
  drupal_exit();
}

/**
 * Page callback function from hook_menu().
 * Only prints the HTML and exits. No themed page is created. Just a partial page for use by the Javascript.
 * e.g. /ask-us/block-unthemed
 */
function libanswers_block_unthemed() {
  print libanswers_get_answers_block();
  drupal_exit();
}
