(function ($) { // JavaScript should be compatible with other libraries than jQuery
  Drupal.behaviors.libanswers = { // D7 "Changed Drupal.behaviors to objects having the methods "attach" and "detach"."
    attach: function(context) {

      // Global.
      var query;

      // Add/remove the default text when they click off/on the input box in the block.
      $("#libanswers-ajax-query").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == 'Don\'t see your question above? Have feedback? Type it here.') {
          this.value = '';
        }
      });
      $("#libanswers-ajax-query").blur(function() {
        if (this.value.length < 1) {
          this.value = 'Don\'t see your question above? Have feedback? Type it here.';
        }
      });

      // On pages where the search box is present, put the descriptive text in the box instead of a heading.
      $("#edit-libanswers-autocomplete").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == 'Search Frequently Asked Questions') {
          this.value = '';
        }
      });
      $("#edit-libanswers-autocomplete").blur(function() {
        if (this.value.length < 1) {
          this.value = 'Search Frequently Asked Questions';
        }
      });

      // Question form labels
      $(".la_askqform_page #pquestion").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == '* Question/Comment') {
          this.value = '';
        }
      });
      $(".la_askqform_page #pquestion").blur(function() {
        if (this.value.length < 1) {
          this.value = '* Question/Comment';
        }
      });
      // --------
      $(".la_askqform_page #pdetails").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == 'More Detail/Explanation') {
          this.value = '';
        }
      });
      $(".la_askqform_page #pdetails").blur(function() {
        if (this.value.length < 1) {
          this.value = 'More Detail/Explanation';
        }
      });
      // --------
      $(".la_askqform_page #pname").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == 'Name') {
          this.value = '';
        }
      });
      $(".la_askqform_page #pname").blur(function() {
        if (this.value.length < 1) {
          this.value = 'Name';
        }
      });
      // --------
      $(".la_askqform_page #pemail").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == '* Email') {
          this.value = '';
        }
      });
      $(".la_askqform_page #pemail").blur(function() {
        if (this.value.length < 1) {
          this.value = '* Email';
        }
      });
      // --------
      $(".la_askqform_page #val4").focus(function() {
        if (this.value.length < 1) {
          this.value = '';
        }
        else if (this.value == 'Library Card Number') {
          this.value = '';
        }
      });
      $(".la_askqform_page #val4").blur(function() {
        if (this.value.length < 1) {
          this.value = 'Library Card Number';
        }
      });

      // Make the form submit submit a live search query.
      $('#libanswers-inner textarea').keypress(function (e) {
        if (e.which == 13) {
          $('#libanswers-ajax-form').submit();
          return false;
        }
      });
      $('#libanswers-ajax-form', context).submit(function() {
        // After the form is submitted, we need to send the contents of the input box as a query to LibAnswers via their Query API.
        query = $('#libanswers-ajax-query', context).val();
        query = encodeURIComponent(query); // html encode it.
        // Get the results from the API call and display them as the new content of the #libanswers-inner div.
        $('#libanswers-inner', context).slideUp(function() {
          $(this).html('<div style="text-align: center; margin-top: 15px;"><img src="/sites/all/modules/contrib/libanswers/images/ajax-loader.gif" alt="Loading" /></div>').slideDown().load('/ask-us/search-results-unthemed?query=' + query, { query : query }, function(data) {
            // Add LibAnswers form when the button is clicked.
            $('#libanswers-inner a#libanswers-please-send', context).click(function() {
              $('#libanswers-inner', context).slideUp(function() {
                // Note: I copied this HTML by viewing source on the embedded widget. Also added the script link at the beginning.
                // Replaced #745 (our widget id) with + Drupal.settings.libanswers.question_form_id
                $(this).html('<script src="https://api2.libanswers.com/1.0/widgets/' + Drupal.settings.libanswers.question_form_id + '"></script><div id="s-la-askform-' + Drupal.settings.libanswers.question_form_id + '" class="s-la-askform s-la-askform-qu-588"><h2 class=la_header>Ask Us a Question or Leave Feedback</h2><div class="s-la-askform-intro">Have a question? We have answers! Leave your question or comment below and we will reply within 24 hours. Please give an e-mail address so we know where to send your answer. We will not share it.</div><form name="s-la-askform-form_' + Drupal.settings.libanswers.question_form_id + '" id="s-la-askform-form_' + Drupal.settings.libanswers.question_form_id + '" method="post"><input type="hidden" name="instid" value="' + Drupal.settings.libanswers.id + '"><input type="hidden" name="quid" value="588"><input type="hidden" name="qlog" value="0"><input type="hidden" name="source" value="4"><fieldset class="s-la-askform-qset"><div class="form-group la_field pquestion_wrap"><label for="pquestion_' + Drupal.settings.libanswers.question_form_id + '" class="control-label"><span class="reqnote">* </span>Question/Comment</label><textarea class="form-control" name="pquestion" id="pquestion_' + Drupal.settings.libanswers.question_form_id + '" aria-required="true"></textarea></div><div class="form-group la_field pdetails_wrap"><label for="pdetails_' + Drupal.settings.libanswers.question_form_id + '" class="control-label">More Detail/Explanation</label><textarea class="form-control" name="pdetails" id="pdetails_' + Drupal.settings.libanswers.question_form_id + '"></textarea></div></fieldset><fieldset class="s-la-askform-infoset"><div class="form-group la_field pname_wrap"><label for="pname_' + Drupal.settings.libanswers.question_form_id + '" class="control-label">Name</label><input class="form-control" type="text" id="pname_' + Drupal.settings.libanswers.question_form_id + '" name="pname" value=""></div><div class="form-group la_field pemail_wrap"><label for="pemail_' + Drupal.settings.libanswers.question_form_id + '" class="control-label"><span class="reqnote">* </span>Email</label><input class="form-control required" type="text" id="pemail_' + Drupal.settings.libanswers.question_form_id + '" name="pemail" value="" aria-required="true"></div><div class="form-group la_field val4_wrap"><label for="val4_' + Drupal.settings.libanswers.question_form_id + '" class="control-label">Library Card Number</label><input class="form-control" type="text" id="val4_' + Drupal.settings.libanswers.question_form_id + '" name="val4" value=""></div></fieldset><div id="s-la-askform-reqnote_' + Drupal.settings.libanswers.question_form_id + '" class="s-la-askform-reqnote">Fields marked with <span class="reqnote"> *</span> are required.</div><div class="form-actions"><button id="s-la-askform-submit-' + Drupal.settings.libanswers.question_form_id + '" class="s-la-askform-button form-submit" type="submit">Submit Your Question</button></div></form></div>').slideDown(function() {
                  if (typeof query !== 'undefined' && query != "") {
                    $('textarea[name="pquestion"]').val(decodeURIComponent(query)); // Fill in the question with whatever they typed.
                  }
                  $('#s-la-askform-form_' + Drupal.settings.libanswers.question_form_id, context).submit(function() {
                    if ($('[name="pquestion"]', context).val() && $('[name="pemail"]', context).val()) { // Make sure they entered a question and an email address.
                      libanswers_reset(); // Reset the form back to the beginning
                    }
                    return false;
                  });

                });
              });
              return false;
            });
          });
        });
        return false;
      });


    }
  };

  /**
   * Pull in the original HTML of the form if a question gets submitted.
   */
  function libanswers_reset() {
    $('#libanswers-inner').slideUp(function() {
      $(this).html('<div style="text-align: center; margin-top: 15px;"><img src="/sites/all/modules/contrib/libanswers/images/ajax-loader.gif" alt="Loading" /></div>').slideDown().load('/ask-us/block-unthemed', function() {
        Drupal.attachBehaviors('#libanswers-inner');
        // Add a success message.
        $('#libanswers-inner h2').first().after('<div id="la_result" style="display: block;">Thank you! We will contact you when the question is answered.</div>');
      });
    });
  }

})(jQuery);
