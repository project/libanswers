// $Id $

Description
-------
The LibAnswers module is used to display answers from your SpringShare LibAnswers instance (libanswers.com) within the context of your site.
The module provides an "Ask a Librarian" page that appears at the url /ask-us.
Blocks are also provided to embed answers and to embed the LibAnswers chat widget in your site.
Be sure to visit the configuration page after installing the module.

This module is maintained by the Virtual Services team at Richland Library (http://www.richlandlibrary.com) in South Carolina.
The coding for the Drupal 7 module was originally written by Mark W. Jarrell (http://drupal.org/user/249768).

Install
-------
Installing the LibAnswers module:

1) Download the LibAnswers module from Drupal.org.

2) Create a LibAnswers account (libanswers.com) if you have not done so already.

3) Make note of your numeric account id (usually a 3 or 4-digit number).
This will be used by the module for pulling in the answers for your library.

4) Also make note of your libanswers domain name. It might be something like http://yourlibrary.libanswers.com.

5) Enable the modules using the Modules administration page (/admin/modules).

6) Configure settings for the module on the LibAnswers settings page (/admin/config/user-interface/libanswers).

Support
-------
If you experience a problem with the module, file a
request or issue on the Booklists issue queue (http://drupal.org/project/issues/libanswers).
DO NOT POST IN THE FORUMS. Posting in the issue queue is a direct line of
communication with the module author.