<?php

/**
 * @file
 * Utilizes the LibAnswers API to show questions and answers to site visitors.
 */


/**
 * Define constants.
 */
define('LIBANSWERS_DOMAIN', variable_get('libanswers_domain'));
define('LIBANSWERS_ID', variable_get('libanswers_id'));
define('LIBANSWERS_FEATURED_TOPIC_ID', variable_get('libanswers_featured_topic_id'));
define('LIBANSWERS_CHAT_CODE', variable_get('libanswers_chat_code'));
define('LIBANSWERS_QUESTION_FORM_CODE', variable_get('libanswers_question_form_code'));
// Also get the question form id number for use in JS.
$form_code = trim(preg_replace('/\s+/', ' ', LIBANSWERS_QUESTION_FORM_CODE)); // Strip line breaks.
preg_match('/widgets\/(.*)"/U', $form_code, $matches); // U makes it an ungreedy match.
$libanswers_question_form_id = $matches[1];
if (empty($libanswers_question_form_id)) {
  preg_match('/s-la-widget-(.*)"/U', $form_code, $matches); // U makes it an ungreedy match.
  $libanswers_question_form_id = $matches[1];
}
define('LIBANSWERS_QUESTION_FORM_ID', $libanswers_question_form_id);

/**
 * Implements hook_permission().
 */
function libanswers_permission() {
  return array(
    'configure libanswers' => array(
      'title' => t('Configure LibAnswers'),
      'description' => t('Allows users to make configuration changes for the LibAnswers block/pages displayed on the site.'),
    ),
  );
}

/**
 * Implements hook_init().
 */
function libanswers_init() {
  // Opted to using page-specific JS embeds below because was getting an error related to zIndex that
  // didn't appear to have a resolution w/ the jQuery Update versions of the CSS/scripts.
  /*drupal_add_library('system', 'ui.widget');
  drupal_add_library('system', 'ui.position');
  drupal_add_library('system', 'ui.autocomplete');*/
  drupal_add_js(array('libanswers' => array('id' => LIBANSWERS_ID, 'domain' => str_replace('http://', '', str_replace('https://', '', LIBANSWERS_DOMAIN)), 'question_form_id' => LIBANSWERS_QUESTION_FORM_ID)), 'setting');
  drupal_add_js(drupal_get_path('module', 'libanswers') . '/libanswers.min.js', 'file');
}

/**
 * Implements hook_block_info().
 */
function libanswers_block_info() {
  $blocks = array();
  $blocks['libanswers'] = array(
    'info' => 'LibAnswers: Featured Answers'
  );
  $blocks['libanswers_chat'] = array(
    'info' => 'LibAnswers: Chat'
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function libanswers_block_view($delta = '') {
  $block = array();

  if ($delta == 'libanswers') {
    $block['subject'] = 'LibAnswers: Featured Answers';
    $block['content'] = libanswers_get_answers_block();
  }
  else if ($delta == 'libanswers_chat') {
    $block['subject'] = 'Ask Us';
    $block['content'] = libanswers_get_chat_block();
  }

  return $block;
}

/**
 * Implements hook_menu().
 */
function libanswers_menu() {
  $items['admin/config/user-interface/libanswers'] = array(
    'title' => t('LibAnswers'),
    'description' => t('Utilizes the LibAnswers API to show questions and answers to site visitors.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('libanswers_settings'),
    'file' => 'libanswers.admin.inc',
    'access arguments' => array('configure libanswers'),
    'type' => MENU_NORMAL_ITEM
  );
  // Add a menu callback that accepts a question ID. Then, run the single question/answer api to determine the page contents.
  // This will display the answer to the user in a page. Should look like libanswers/%
  $items['ask-us/answer/%'] = array(
    'title' => t('Ask a Librarian'),
    'page callback' => 'libanswers_page_get_answer',
    'page arguments' => array(2),
    'file' => 'libanswers.pages.inc', // Move page callback functions to an include file or else they're called on each page load.
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  // A simple page to display the search form and a tag cloud
  $items['ask-us'] = array(
    'title' => t('Ask a Librarian'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('libanswers_search_form'),
    'file' => 'libanswers.pages.inc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  $items['ask-us/search-results'] = array(
    'title' => t('Search Results'),
    'page callback' => 'libanswers_search_results',
    'file' => 'libanswers.pages.inc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  $items['ask-us/search-results-unthemed'] = array(
    'title' => t('Search Results'),
    'page callback' => 'libanswers_search_results_unthemed',
    'file' => 'libanswers.pages.inc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  $items['ask-us/topic'] = array(
    'title' => t('Topics'),
    'page callback' => 'libanswers_page_get_topic_list',
    'file' => 'libanswers.pages.inc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  $items['ask-us/topic/%'] = array(
    'title' => t('Topic Answers'),
    'title callback' => 'libanswers_page_get_topic_title',
    'title arguments' => array(2),
    'page callback' => 'libanswers_page_get_topic_answers',
    'page arguments' => array(2),
    'file' => 'libanswers.pages.inc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  $items['ask-us/block-unthemed'] = array(
    'title' => t('Ask Us'),
    'page callback' => 'libanswers_block_unthemed',
    'file' => 'libanswers.pages.inc',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK
  );
  return $items;
}

/**
 * Builds the content of the basic block. We need to separate this out so that
 * it can be called as either block content or unthemed page content for use in
 * the Javascript.
 */
function libanswers_get_answers_block() {
  $questions = '';
  // Pull from 24 hr cache.
  $cid = 'libanswers:featured-5';
  $cached = cache_get($cid, 'cache');
  if ($cached && time() < $cached->expire) {
    $questions_json = $cached->data;
  }
  else {
    // Not in cache. Let's run the query.
    // Make API call to pull in first 5 answers.
    $questions_json = file_get_contents('https://api2.libanswers.com/1.0/faqs?iid=' . LIBANSWERS_ID . '&topic_id=' . LIBANSWERS_FEATURED_TOPIC_ID . '&limit=5&sort=totalhits&sort_dir=desc');
    cache_set($cid, $questions_json, 'cache', time() + 86400); // Cache the query result for 24 hours.
  }
  $questions_object = json_decode($questions_json);
  if (!empty($questions_object->faqs)) {
    $questions .= '<ul class="la_anslist">';
    foreach ($questions_object->faqs as $question) {
      $questions .= '<li><a class="la_question" href="/ask-us/answer/' . $question->faqid . '">' . $question->question . '</a></li>';
    }
    $questions .= '</ul>';
  }
  $block = '<div id="libanswers-inner">
    <h2>FAQs:</h2>
    ' . $questions . '
    <ul><li><a href="/ask-us">More &raquo;</a></li></ul>
    <form id="libanswers-ajax-form">
      <textarea class="form-textarea" rows="2" cols="10" id="libanswers-ajax-query">Don\'t see your question above? Have feedback? Type it here.</textarea>
      <input type="submit" class="form-submit" value="Submit">
    </form>
  </div>';
  return $block;
}

/**
 * Builds the content of the chat block.
 */
function libanswers_get_chat_block() {
  return LIBANSWERS_CHAT_CODE;
}

/**
 * Builds the tag cloud.
 */
function libanswers_get_tag_cloud() {
  // Use API instead of widget. Because widget links can't be changed to point to local site. They're built via Javascript code.
  // Pull from 24 hr cache.
  $cid = 'libanswers:topics';
  $cached = cache_get($cid, 'cache');
  if ($cached && time() < $cached->expire) {
    $topics_json = $cached->data;
  }
  else {
    // Not in cache. Let's run the query.
    $topics_json = file_get_contents('https://api2.libanswers.com/1.0/topics?iid=' . LIBANSWERS_ID);
    cache_set($cid, $topics_json, 'cache', time() + 86400); // Cache the query result for 24 hours.
  }
  $tags_object = json_decode($topics_json);
  $tags = $tags_object->topics;
  $output = '';
  if (!empty($tags)) {
    $output .= '<h2>Browse by Popular Topics</h2>';
    $min_tag_count = 1000; // Start high and let it decrease.
    $max_tag_count = 1; // Start low and let it increase.
    foreach($tags as $key => $tag) {
      if ($tag->count > $max_tag_count) {
        $max_tag_count = $tag->count; // This is the new maximum!
      }
      if ($tag->count < $min_tag_count) {
        $min_tag_count = $tag->count; // This is the new minimum!
      }
    }
    $counter = 0;
    foreach($tags as $key => $tag) {
      // Tag font size should be from 80% to 150% based on comparison to the min/max values.
      // The mathematical formula to derive the necessary font size:
      // Example 1: Current = 4, min = 2, max = 18
      // ---------------------(n - min) / (max - min) = percentage of the 70 that we need.
      // ---------------------(4 - 2) / (18 - 2) = .125
      // ---------------------0.125 * 70 = 8.75
      // ---------------------80 + 12.5 = 92.5 (final font percentage size)
      $font_size = ($tag->count - $min_tag_count) / ($max_tag_count - $min_tag_count) * 70 + 80;
      if ($counter > 0) { $output .= ' <span class="topicseparator">&nbsp;&ndash;&nbsp;</span> '; }
      $output .= '<a href="/ask-us/topic/' . $tag->topic_id . '" style="font-size: ' . $font_size . '%;">' . $tag->name . '</a>';
      $counter++;
    }
  }
  return $output;
}

/**
 * Builds the in-page question form.
 */
function libanswers_get_question_form($header = TRUE) {
  $output = '<div id="libanswers-form-wrapper">';
  $output .= '<script src="//api2.libanswers.com/1.0/widgets/' . LIBANSWERS_QUESTION_FORM_ID . '"></script><div id="s-la-askform-' . LIBANSWERS_QUESTION_FORM_ID . '" class="s-la-askform s-la-askform-qu-588">';
  if ($header == TRUE) {
    $output .= '<h2 class="la_header">' . t('Ask Us a Question or Leave Feedback') . '</h2>';
  }
  else {
    $output .= '<h2 class="la_header">' . t('Not the answer you\'re looking for? Submit your question here.') . '</h2>';
  }
  $output .= '<div class="s-la-askform-intro">Have a question? We have answers! Leave your question or comment below and we will reply with 24 hours. Please give an e-mail address so we know where to send your answer. We will not share it.</div><form name="s-la-askform-form_' . LIBANSWERS_QUESTION_FORM_ID . '" id="s-la-askform-form_' . LIBANSWERS_QUESTION_FORM_ID . '" class="la_askqform_page" method="post"><input type="hidden" name="instid" value="' . LIBANSWERS_ID . '"><input type="hidden" name="quid" value="588"><input type="hidden" name="qlog" value="0"><input type="hidden" name="source" value="4"><!--<fieldset class="s-la-askform-qset">--><div class="form-group form-item la_field pquestion_wrap"><label for="pquestion_' . LIBANSWERS_QUESTION_FORM_ID . '" class="control-label"><span class="reqnote">* </span>Question/Comment</label><textarea class="form-control" name="pquestion" id="pquestion_' . LIBANSWERS_QUESTION_FORM_ID . '" aria-required="true"></textarea></div><div class="form-group form-item la_field pdetails_wrap"><label for="pdetails_' . LIBANSWERS_QUESTION_FORM_ID . '" class="control-label">More Detail/Explanation</label><textarea class="form-control" name="pdetails" id="pdetails_' . LIBANSWERS_QUESTION_FORM_ID . '"></textarea></div><!--</fieldset><fieldset class="s-la-askform-infoset">--><div class="form-group form-item la_field pname_wrap"><label for="pname_' . LIBANSWERS_QUESTION_FORM_ID . '" class="control-label">Name</label><input class="form-control" type="text" id="pname_' . LIBANSWERS_QUESTION_FORM_ID . '" name="pname" value=""></div><div class="form-group form-item la_field pemail_wrap"><label for="pemail_' . LIBANSWERS_QUESTION_FORM_ID . '" class="control-label"><span class="reqnote">* </span>Email</label><input class="form-control required" type="text" id="pemail_' . LIBANSWERS_QUESTION_FORM_ID . '" name="pemail" value="" aria-required="true"></div><div class="form-group form-item la_field val4_wrap"><label for="val4_' . LIBANSWERS_QUESTION_FORM_ID . '" class="control-label">Library Card Number</label><input class="form-control" type="text" id="val4_' . LIBANSWERS_QUESTION_FORM_ID . '" name="val4" value=""></div><!--</fieldset>--><div id="s-la-askform-reqnote_' . LIBANSWERS_QUESTION_FORM_ID . '" class="s-la-askform-reqnote">Fields marked with <span class="reqnote"> *</span> are required.</div><div class="form-actions"><button id="s-la-askform-submit-' . LIBANSWERS_QUESTION_FORM_ID . '" class="form-submit" type="submit">Submit Your Question</button></div></form></div>';
  $output .= '</div>';
  return $output;
}
